package com.example.englishwordcards.core.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class Preferences(val context: Context) {

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    var limitActions: Int
        get(){
            return getIntByKey(PrefKeys.LIMIT_ACTIONS)
        }
        set(value){
            setIntByKey(PrefKeys.LIMIT_ACTIONS, value)
        }

    private fun getIntByKey(key: String): Int = sharedPreferences.getInt(key, 5)
    private fun setIntByKey(key: String, value: Int){
        sharedPreferences
            .edit()
            .putInt(key, value)
            .apply()
    }

    var countTransBtnClick: Int
        get(){
            return getCountTransBtnClickByKey(PrefKeys.ADDED_WORD_COUNT)
        }
        set(value){
            setCountTransBtnClickByKey(PrefKeys.ADDED_WORD_COUNT, value)
        }

    private fun getCountTransBtnClickByKey(key: String): Int = sharedPreferences.getInt(key, 0)
    private fun setCountTransBtnClickByKey(key: String, value: Int){
        sharedPreferences
            .edit()
            .putInt(key, value)
            .apply()
    }

    var countAddedWord: Int
        get(){
            return getCountAddedWordKey(PrefKeys.TRANSLATE_COUNT)
        }
        set(value){
            setCountAddedWordByKey(PrefKeys.TRANSLATE_COUNT, value)
        }

    private fun getCountAddedWordKey(key: String): Int = sharedPreferences.getInt(key, 0)
    private fun setCountAddedWordByKey(key: String, value: Int){
        sharedPreferences
            .edit()
            .putInt(key, value)
            .apply()
    }

    var noLimits: Boolean
        get(){
            return getBooleanByKey(PrefKeys.NO_LIMITS)
        }
        set(value){
            setBooleanByKey(PrefKeys.NO_LIMITS, value)
        }

    private fun getBooleanByKey(key: String): Boolean = sharedPreferences.getBoolean(key, false)
    private fun setBooleanByKey(key: String, value: Boolean){
        sharedPreferences
            .edit()
            .putBoolean(key, value)
            .apply()
    }

    fun reset(){
        noLimits = false
        countAddedWord = 0
        countTransBtnClick = 0
    }

    object PrefKeys {
        var porVersion = false
        const val NO_LIMITS = "NO_LIMITS"
       const val LIMIT_ACTIONS = "LIMIT_ACTIONS"
        const val ADDED_WORD_COUNT = "ADDED_WORD_COUNT"
        const val TRANSLATE_COUNT = "TRANSLATE_COUNT"
    }
}