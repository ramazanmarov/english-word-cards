package com.example.englishwordcards.core.coustom

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.englishwordcards.core.base.BaseFragment
import com.example.englishwordcards.core.storage.Preferences
import com.example.englishwordcards.databinding.PopUpDialogBinding
import com.example.englishwordcards.ui.main.MainActivity
import kotlinx.coroutines.delay
import java.util.Timer

class PopUpDialog: DialogFragment() {
    lateinit var prefs: Preferences
    lateinit var mainActivity: MainActivity
    lateinit var binding: PopUpDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PopUpDialogBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = Preferences(requireContext())
        setupView()
        initListeners()
    }

    private fun setupView()= with(binding){

    }

    private fun hideDialog() = mainActivity.hideDialog()

    private fun initListeners()= with(binding){
        btnClose.setOnClickListener {
            hideDialog()
        }

        btnNoThanks.setOnClickListener {
            hideDialog()
        }

        btnFreeTrial.setOnClickListener {
//            var timer = delay(600)
        }

        rbMonth.setOnClickListener{

        }

        btnBuy.setOnClickListener {
            prefs.noLimits = true
            Toast.makeText(requireContext(), "Вы купили подписку!", Toast.LENGTH_SHORT).show()
            hideDialog()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as MainActivity).let { mainActivity = it }
    }

}