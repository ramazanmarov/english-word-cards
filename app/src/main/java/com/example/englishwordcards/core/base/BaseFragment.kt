package com.example.englishwordcards.core.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.englishwordcards.core.storage.Preferences
import com.example.englishwordcards.ui.main.MainActivity

abstract class BaseFragment<VB: ViewBinding> : Fragment() {
    private lateinit var mainActivity: MainActivity

    open lateinit var prefs: Preferences
    open lateinit var binding: VB
    protected abstract fun getViewBinding(): VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()
        prefs = Preferences(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    fun showDialog() = mainActivity.showDialog()
    fun hideDialog() = mainActivity.hideDialog()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as MainActivity).let { mainActivity = it}
    }

}