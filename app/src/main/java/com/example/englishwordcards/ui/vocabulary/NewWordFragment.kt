package com.example.englishwordcards.ui.vocabulary

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.view.View
import androidx.navigation.Navigation
import com.example.englishwordcards.R
import com.example.englishwordcards.core.base.BaseFragment
import com.example.englishwordcards.databinding.FragmentNewWordBinding
import com.example.englishwordcards.db.models.WordModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.Date

class NewWordFragment : BaseFragment<FragmentNewWordBinding>() {

    private val viewModel by viewModel<WordViewModel>()

    override fun getViewBinding() = FragmentNewWordBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }
    private fun initListeners(){
        binding.btnAdd.setOnClickListener {
            when (prefs.noLimits) {
                true -> {
                    insertDataToDB()
                }
                else -> {
                    if(prefs.countAddedWord == prefs.limitActions){
                        showDialog()
                    }else {
                        prefs.countAddedWord++
                        insertDataToDB()
                    }
                }
            }
        }

        binding.btnBack.setOnClickListener {
            backPressed()
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun insertDataToDB(){
        val englishWord = binding.etEnglishWord.text.toString()
        val transWord = binding.etTransWord.text.toString()
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())

        if (inputCheck(englishWord, transWord)){
            val newWordModel = WordModel(0, englishWord, transWord, currentDate, false)
            viewModel.addNewWord(newWordModel)
            backPressed()
        }
    }

    private fun inputCheck(englishWord: String, transWord: String): Boolean{
        return !(isEmpty(englishWord) && isEmpty(transWord))
    }

    private fun backPressed(){
        view?.let {
            Navigation.findNavController(it).navigate(R.id.action_newWordFragment_to_vocabularyFragment)
        }
    }

}