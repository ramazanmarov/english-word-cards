package com.example.englishwordcards.ui.vocabulary

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.englishwordcards.R
import com.example.englishwordcards.core.base.BaseFragment
import com.example.englishwordcards.databinding.FragmentVocabularyBinding
import com.example.englishwordcards.db.models.WordModel
import com.example.englishwordcards.db.models.fakeData
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.Locale

class VocabularyFragment : BaseFragment<FragmentVocabularyBinding>(),
    ItemWordAdapter.ItemClickListener, SearchView.OnQueryTextListener {

    private val viewModel by viewModel<WordViewModel>()
    private lateinit var adapter: ItemWordAdapter
    override fun getViewBinding() = FragmentVocabularyBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setupView() = with(binding) {
        adapter = ItemWordAdapter(this@VocabularyFragment)
        rvWordList.adapter = adapter
        rvWordList.layoutManager = LinearLayoutManager(context)
        showAllData()

        searchView.isSubmitButtonEnabled = true
        searchView.setOnQueryTextListener(this@VocabularyFragment)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        return true
    }

    override fun onQueryTextChange(query: String): Boolean {
        if (query != null) {
            searchDatabase(query)
        }
        return true
    }

    private fun searchDatabase(query: String) {
        val searchQuery = "%$query%"

        viewModel.searchDatabase(searchQuery).observe(viewLifecycleOwner) { list ->
            list.let {
                adapter.setFilteredList(it)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showAllData(){
        binding.dialog.isVisible = true
        viewModel.showAllData.observe(viewLifecycleOwner, Observer {
                if(it.isNullOrEmpty()){
                    binding.dialog.isVisible = false
                }else {
                    adapter.items = it
                    binding.dialog.isVisible = false
                }
            adapter.notifyDataSetChanged()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initListeners() {
        binding.btnAdd.setOnClickListener {
            view?.let { it1 ->
                Navigation.findNavController(it1)
                    .navigate(R.id.action_vocabularyFragment_to_newWordFragment)
            }
        }

        binding.swipeRef.setOnRefreshListener {
            viewModel.showAllData.observe(viewLifecycleOwner, Observer {
                if(it.isNullOrEmpty()){
                    binding.swipeRef.isRefreshing = false
                }else {
                    adapter.items = it
                    binding.swipeRef.isRefreshing = false
                }
                adapter.notifyDataSetChanged()
            })
        }
    }
}