package com.example.englishwordcards.ui.vocabulary

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.englishwordcards.db.models.WordModel
import com.example.englishwordcards.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WordViewModel(private val repo: Repository): ViewModel() {

    val showAllData: LiveData<MutableList<WordModel>> = repo.readAllData

    fun addNewWord(itemModel: WordModel) {
        viewModelScope.launch(Dispatchers.IO){
            repo.addNewWord(itemModel)
        }
    }

    fun searchDatabase(searchQuery: String): LiveData<MutableList<WordModel>> {
        return repo.searchDatabase(searchQuery)
    }
}