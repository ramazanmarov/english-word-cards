package com.example.englishwordcards.ui.vocabulary

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.englishwordcards.R
import com.example.englishwordcards.databinding.ItemCardWordBinding
import com.example.englishwordcards.databinding.ItemWordListBinding
import com.example.englishwordcards.db.models.WordModel

class ItemWordAdapter(private val clickListener: ItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: MutableList<WordModel> = mutableListOf()

    interface ItemClickListener {
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFilteredList(filteredList: MutableList<WordModel>){
        this.items = filteredList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val binding: ItemWordListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var index: Int = 0
        private lateinit var item: WordModel

        fun bind(_item: WordModel, position: Int) {
            item = _item
            index = position

            item.apply {
                binding.tvWordAtEng.text = item.englishWord.toString()
                binding.tvWordAtRuss.text = item.russianWord.toString()
                binding.tvDateCreate.text = item.date.toString()
            }
        }

        companion object {
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val binding =
                    ItemWordListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return Holder(binding).apply {
                    itemView.setOnClickListener {
                    }
                }
            }
        }
    }
}