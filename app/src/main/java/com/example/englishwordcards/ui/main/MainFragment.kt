package com.example.englishwordcards.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.example.englishwordcards.core.base.BaseFragment
import com.example.englishwordcards.databinding.FragmentMainBinding
import com.example.englishwordcards.databinding.ItemCardWordBinding
import com.example.englishwordcards.db.models.WordModel
import com.example.englishwordcards.ui.vocabulary.WordViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<FragmentMainBinding>(),
    ViewPagerWordAdapter.ItemClickListener {

    private lateinit var adapter: ViewPagerWordAdapter
    private val viewModel by viewModel<WordViewModel>()
    override fun getViewBinding() = FragmentMainBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initListeners() = with(binding) {
        btnNext.setOnClickListener {
            vpCard.currentItem = vpCard.currentItem + 1
        }

        binding.swipeRef.setOnRefreshListener {
            viewModel.showAllData.observe(viewLifecycleOwner, Observer {
                if(it.isNullOrEmpty()){
                    binding.swipeRef.isRefreshing = false
                }else {
                    adapter.items = it
                    binding.swipeRef.isRefreshing = false
                }
                adapter.notifyDataSetChanged()
            })
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupView() = with(binding) {
        adapter = ViewPagerWordAdapter(this@MainFragment)
        vpCard.adapter = adapter
        adapter.notifyDataSetChanged()
        getData()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getData(){
        binding.dialog.isVisible = true
        viewModel.showAllData.observe(viewLifecycleOwner, Observer {
            if(it.isNullOrEmpty()){
                binding.dialog.isVisible = false
            }else {
                adapter.items = it
                binding.dialog.isVisible = false
            }
            adapter.notifyDataSetChanged()
        })
//        adapter.items = fakeData.getList()
    }

    override fun onTranslateClick(binding: ItemCardWordBinding, item: WordModel, index: Int) = with(binding) {
        when (prefs.noLimits) {
            true -> {
                adapter.changeVisibleFlag(index)
            }
            else -> {
                if (prefs.countTransBtnClick >= prefs.limitActions) {
                    showDialog()
                } else {
                    prefs.countTransBtnClick++
                    adapter.changeVisibleFlag(index)
                }
            }
        }
    }
}