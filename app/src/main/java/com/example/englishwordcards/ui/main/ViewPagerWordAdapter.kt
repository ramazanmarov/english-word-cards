package com.example.englishwordcards.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.englishwordcards.R
import com.example.englishwordcards.databinding.ItemCardWordBinding
import com.example.englishwordcards.db.models.WordModel

class ViewPagerWordAdapter(private val clickListener: ItemClickListener):
    RecyclerView.Adapter<ViewPagerWordAdapter.ViewPagerWordViewHolder>() {

    open class ViewPagerWordViewHolder(view: View) : RecyclerView.ViewHolder(view)

    var items: MutableList<WordModel> = mutableListOf()

    interface ItemClickListener {
        fun onTranslateClick(binding: ItemCardWordBinding, item: WordModel, index: Int)
    }

    fun changeVisibleFlag(index: Int){
        items[index].isTranslateVisible = !items[index].isTranslateVisible
        notifyItemChanged(index)
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerWordViewHolder =
        when (viewType) {
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }

    override fun onBindViewHolder(holder: ViewPagerWordViewHolder, position: Int) {
        when (holder) {
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : ViewPagerWordViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val binding: ItemCardWordBinding) :
        ViewPagerWordViewHolder(binding.root) {
        private var index: Int = 0
        private lateinit var item: WordModel

        @SuppressLint("ResourceAsColor")
        fun bind(_item: WordModel, position: Int) {
            item = _item
            index = position

            item.apply {
                binding.tvEnglishWord.text = item.englishWord
                binding.tvTransWord.text = item.russianWord
                binding.tvTransWord.isVisible = item.isTranslateVisible
                binding.tvTranslate.isVisible = item.isTranslateVisible
                binding.btnTranslate.isEnabled = !item.isTranslateVisible
            }
        }

        companion object {
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val binding =
                    ItemCardWordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return Holder(binding).apply {
                    binding.btnTranslate.setOnClickListener {
                        clickListener.onTranslateClick(binding, item, index)
                    }
                }
            }
        }
    }
}