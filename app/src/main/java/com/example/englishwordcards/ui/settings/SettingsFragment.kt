package com.example.englishwordcards.ui.settings

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import com.example.englishwordcards.core.base.BaseFragment
import com.example.englishwordcards.databinding.FragmentSettingsBinding

class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {
    override fun getViewBinding() = FragmentSettingsBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners(){
        binding.btnReset.setOnClickListener {
            showMessageDialog("Вы действительно хотите отменить подписку?"){
                prefs.reset()
            }
        }
    }

    private fun showMessageDialog(message: String, okLabel: String = "Отмена",
                                  canceLabel: String = "Подтвердить",
                                  onClick: (() -> Unit) = {}, onCancelClick: (() -> Unit) = {}){
        AlertDialog.Builder(requireContext())
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(okLabel){_,_ -> onClick()}
            .setNegativeButton(canceLabel){_,_ -> onCancelClick()}
            .create()
            .show()
    }
}