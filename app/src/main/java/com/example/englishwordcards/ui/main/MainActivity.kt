package com.example.englishwordcards.ui.main

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.englishwordcards.R
import com.example.englishwordcards.core.coustom.PopUpDialog
import com.example.englishwordcards.core.storage.Preferences
import com.example.englishwordcards.databinding.ActivityMainBinding

class MainActivity : FragmentActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var alertDialog : PopUpDialog
    lateinit var prefs: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        prefs = Preferences(this)
        initNavGraph()
        setupViews()
    }

    private fun setupViews(){
        if(prefs.countAddedWord == prefs.limitActions || prefs.countTransBtnClick == prefs.limitActions){
            showDialog()
        }
    }

     fun showDialog() {
             alertDialog = PopUpDialog()
             alertDialog.show(supportFragmentManager, "popUp")
     }

     fun hideDialog(){
        alertDialog.dismiss()
    }

    private fun initNavGraph(){
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView)
                as NavHostFragment
        val navHostController = navHostFragment.navController

        binding.bottomMenu.setupWithNavController(navHostController)

        navHostController.addOnDestinationChangedListener{_, destination, _->
            when(destination.id){
                R.id.mainFragment,
                R.id.vocabularyFragment,
                R.id.settingsFragment -> binding.bottomMenu.isVisible = true
                else -> binding.bottomMenu.visibility = View.GONE
            }
        }
    }
}