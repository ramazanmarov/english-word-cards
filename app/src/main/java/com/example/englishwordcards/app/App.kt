package com.example.englishwordcards.app

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.example.englishwordcards.core.coustom.LocaleHelper
import com.example.englishwordcards.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@App)
            modules(appModule)
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base!!))
    }

}