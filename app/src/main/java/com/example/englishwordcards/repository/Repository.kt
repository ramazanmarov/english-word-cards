package com.example.englishwordcards.repository

import androidx.lifecycle.LiveData
import com.example.englishwordcards.db.models.Dao
import com.example.englishwordcards.db.models.WordModel

class Repository(private val dao: Dao) {

    val readAllData: LiveData<MutableList<WordModel>> = dao.showAllWords()

    suspend fun addNewWord(wordModel: WordModel){
        dao.addNewWord(wordModel)
    }

    fun searchDatabase(searchQuery: String): LiveData<MutableList<WordModel>> {
        return dao.searchDatabase(searchQuery)
    }
}