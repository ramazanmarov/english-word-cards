package com.example.englishwordcards.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "word_table")
data class WordModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,
    val englishWord: String,
    val russianWord: String,
    val date: String,
    var isTranslateVisible: Boolean
):Serializable

data class ItemWord(
    val englishWord: String,
    val russianWord: String,
    val date: String,
    var isTranslateVisible: Boolean
):Serializable
