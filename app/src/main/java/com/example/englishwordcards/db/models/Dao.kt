package com.example.englishwordcards.db.models

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addNewWord(word: WordModel)

    @Query("SELECT * FROM word_table")
    fun showAllWords(): LiveData<MutableList<WordModel>>

    @Query("SELECT * FROM word_table WHERE englishWord LIKE :searchQuery " +
            "OR russianWord LIKE :searchQuery OR date LIKE :searchQuery")
    fun searchDatabase(searchQuery: String): LiveData<MutableList<WordModel>>

}