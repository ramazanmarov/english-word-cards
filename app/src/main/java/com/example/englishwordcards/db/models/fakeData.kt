package com.example.englishwordcards.db.models

object fakeData {

    val list = mutableListOf<ItemWord>(
        ItemWord("Simple", "Простой", "08.09.2023 16:45", false),
        ItemWord("Red", "Красный", "08.09.2023 16:45", false),
        ItemWord("Green", "Зеленый", "08.09.2023 16:45", false),
        ItemWord("Pencil", "Ручка", "08.09.2023 16:45", false),
        ItemWord("Book", "Книга", "08.09.2023 16:45", false),
        ItemWord("Rock", "Скала", "08.09.2023 16:45", false),
        ItemWord("Table", "Стол", "08.09.2023 16:45", false)
    )


    @JvmName("getList1")
    fun getList(): MutableList<ItemWord> = list
}
