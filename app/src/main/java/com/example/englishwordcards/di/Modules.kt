package com.example.englishwordcards.di

import androidx.room.Room
import com.example.englishwordcards.db.models.WordDB
import com.example.englishwordcards.repository.Repository
import com.example.englishwordcards.ui.vocabulary.WordViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel { WordViewModel(get()) }

    factory { Repository(get()) }
    // Room Database
    single {
        Room.databaseBuilder(
            androidApplication(),
            WordDB::class.java,
            "WORD_CARDS_DB"
        ).build()
    }

    single { get<WordDB>().wordDao() }
}