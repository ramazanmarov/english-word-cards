

plugins {
    id("com.android.application") version "7.3.0" apply false
    id("org.jetbrains.kotlin.android") version "1.8.0" apply false
    id("com.google.devtools.ksp") version "1.9.10-1.0.13" apply false

}

buildscript {
    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.1")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:2.7.2")
        //noinspection GradlePluginVersion
    }
}



